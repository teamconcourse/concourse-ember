import {module, test} from 'qunit';
import {setupRenderingTest} from 'ember-qunit';
import {render} from '@ember/test-helpers';
import hbs from 'htmlbars-inline-precompile';

module('Integration | Component | concourse-ci', function (hooks) {
  setupRenderingTest(hooks);
  test('display given text', async function (assert) {
    await render(hbs`{{concourse-ci text="my test"}}`);
    assert.equal(this.element.textContent.trim(), 'my test');
  });

  test("quick maths", async function (assert) {
    await render(hbs`{{concourse-ci num1=2 num2=2}}`);
    assert.equal(this.element.textContent.trim(), '2+2=4');
  })
});
