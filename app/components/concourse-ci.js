import Component from '@ember/component';
import {computed} from "@ember/object";

export default Component.extend({
  quickMaths: computed("num1", "num2", function () {
    if (this.get("num1") && this.get("num2")) {
      return this.get("num1") + this.get("num2");
    }
    return "";
  })
});
